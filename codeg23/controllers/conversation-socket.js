var OrderSchema = require('../models/order');
var MenuSchema = require('../models/menu');
var MessageSchema = require('../models/message');
var moment = require('moment');
/*
This controller is used for socket io specifically.
*/
module.exports = function(io){
	// Listen to 'connect' event, which used for conversation
	io.on('connection', function(socket){
		socket.on('conversation', function(msg){
			console.log("Socket!");
			var now = moment();
			var pieces = msg.split('_');
			var conversationid = pieces[0];
			var userid = pieces[1];
			var message = pieces[2];
			record = new MessageSchema({
				user_id : userid,
				timestamp : now,
				conversation_id : conversationid,
				content : message
			})
			record.save(function(err){
				if(err){
					console.log(err);
				}
				// Emit message to specific conversation
				io.emit('conversation-'+conversationid,userid+message);
			});
  		});
	});
}