var express = require('express');
var router = express.Router();
var FoodSchema = require('../models/food');
var MenuSchema = require('../models/menu')
var FoodgallerySchema = require('../models/foodgallery');
var moment = require('moment');
var ObjectId = require('mongoose').Types.ObjectId;


/* Get profile page */
router.get('/viewall', function(req, res) {
	//find food by gallery id
	FoodgallerySchema.findOne({user_id : req.user._id},function(err, gallery){
		FoodSchema.find({foodgallery_id : gallery._id},function(err, _foods){
			res.render('foodgallery',{
				user : req.user,
				foods : _foods
			});
        });
	});	
});

router.get('/addfood/:id', function(req, res) {
	var user = req.user;
	var menu_id = req.params.id;
	console.log(user);
	MenuSchema.findOne({_id:menu_id},function(err, menu){
		res.render('addfood',{
			user : req.user,
			menu : menu
		});
	})
});

router.post('/addfood', function(req, res) {

	var _img = req.files;
	var _user_id = req.user._id;
	var _menu_id = req.body.menu_id;

	MenuSchema.findOne({_id : _menu_id},function(err, menu){
		imgpaths = menu.imgpaths;
		imgpaths.push('/uploads/' + req.files.foodimg.name);
		menu.imgpaths = imgpaths;
		menu.save(function(err) {
            if (err){   
                throw err;  
           	}
			res.redirect('/menu/detail/'+_menu_id);
        });
	});	
});

module.exports = router;
