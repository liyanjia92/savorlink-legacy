var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer');
var fs = require('fs');
//This transporter is used for node mailer
var transporter = nodemailer.createTransport({
	service:'gmail',
	auth: {
		user:'cornell.codeg23@gmail.com',
		pass:'2015codeg23'
	}
});

// Handle the GET request for contact page
router.get('/', function(req, res) {
	res.render('contact',{
		user : req.user,
		success: false
	});
 });

// Handle the POST request for contact page
// This function will send a client's message to contact email address
router.post('/', function(req, res) {
	var _name = req.body.contactName;
	var _email = req.body.contactEmail;
	var _msg = req.body.contactMessage;
	transporter.sendMail({
		from: 'cornell.gatesg23@gmail.com',
		to: 'yl2493@cornell.edu',
		subject: 'Contact Message From '+_name,
		text: 'Email: '+_email+'\n'+_msg
	});
	res.render('contact',{
		user : req.user,
		success: true
	});
})



module.exports = router;
