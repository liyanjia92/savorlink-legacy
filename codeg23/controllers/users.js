var express = require('express');
var router = express.Router();
var UserSchema = require('../models/user');
var ReviewSchema = require('../models/review')

// Handle the GET request for public profile page
router.get('/detail/:id', function(req, res) {
	var userId = req.params.id;
	UserSchema.findOne({_id : userId},function(err, target){
		ReviewSchema.find({receiver_id:userId}, function(err, reviews){
			res.render('users',{
				user : req.user,
				target : target,
				reviews : reviews
			});
		});
	})
 });

module.exports = router;
